@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="card">
			<div class="card-header">CREAR PASTEL</div>

			<div class="card-body">
				<form method="POST" action="{{  route('tareas.store')  }}">
					{{ csrf_field() }}
					<div class="form-group">
						<label>Nombre del Cumpleañero</label>
						<input type="text" name="title" class="form-control" required="">
					</div>

					<div class="form-group">
						<label>Fecha de Cumpleaños</label>
						<input type="date" name="deadline" class="form-control">
					</div>

					<div class="form-group">
						<label>Especificaciones</label>
						<textarea class="form-control" name="description" rows="5"></textarea>
					</div>

					<div class="form-group">
						<label>Sabor</label>
							<select class="form-select"  name="sabor" aria-label="Default select example">
							<option selected>Selecciona tu sabor</option>
							<option value="Vainilla" id="1">Vainilla</option>
							<option value="Fresa" id="2">Fresa</option>
							<option value="Chocolate" id="3">Chocolate</option>
							</select>
					</div>

					<div class="form-group">
				    <label for="exampleFormControlSelect1">Selecciona usuario</label>
				    <select class="form-control" id="exampleFormControlSelect1" name="user_id">
				    	@foreach($users as $user)
				      <option value="{{ $user->id }}">{{ $user->name }}</option>
				        @endforeach
				    </select>
				  </div>
					<button type="submit" class="btn btn-primary">Guardar Pastel</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
<script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>