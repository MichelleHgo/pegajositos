<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href= "{{asset('css/bootstrap.min.css')}}">
      
     <!-- Animate style CSS -->  
     <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />  
     <!-- Galería de estilos FancyBox -->  
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
      
    <!-- OWL CAROUSEL-->
    
    <link rel="stylesheet" href= "{{asset('css/owl.carousel.css')}}">  
    <link rel="stylesheet" href= "{{asset('css/owl.theme.default.css')}}">  

 <!-- GOOGLE FONTS --> 
 <link rel="preconnect" href="https://fonts.gstatic.com">
<!-- Tipografías de google fonts -->       
<link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Didact+Gothic&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
      
<!-- JS, popper, js, and JQuery -->  
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>  
    <script src="{{asset('js/bootstrap.min.js')}}"></script>

    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

<!-- OWL CAROUSEL-->   
<script src="{{asset('js/owl.carousel.js')}}"></script>

      
<!-- Iconos fontawesome-->       
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">  

<!-- Pag de estilos propios-->      
<link rel=StyleSheet href="{{asset('css/menuestilo.css')}}" type="text/css" media=screen>       
      
      
      
</head>
<div class="wrapper">
<nav id="sidebar">
         <div class="sidebar-header" href="{{ route('home')}}">
           <a href="{{ route('home')}}"> <img  class="img-fluid michotitulo animate__animated animate__shakeX" src="img/logo.png" >
       </a> </div>

        <ul class="list-unstyled components">
        <li>
                <a href="{{ route('home')}}"><i class="fas fa-home"></i> Inicio</a>
            </li>
           
          <li>
                <a href="{{ route('proyectos.index')}}"><i class="fas fa-book-open"></i> Catálogo</a>
            </li>
            <li>
                <a href="{{ route('tareas.index')}}"><i class="fas fa-birthday-cake"></i> Crea tu pastel</a>
            
              </li>
              @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
 <!--<li>
                <a href="#"><i class="far fa-address-book"></i> Contacto</a>-->
        </ul>

    </nav>
   
    <div id="content">   <!-- CONTENIDO DE LA PAG -->
        
<div id="content"><!-- BOTÓN PARA OCULTAR O MOSTRAR MENÚ --> 

    <nav class="navbar ">
        <div class="container-fluid">

            <button type="button" id="sidebarCollapse" class="botonmenu btn">
                <i class="fas fa-bullseye"></i>
                <span> </span>
            </button>

        </div>
    </nav>   
    </div>  
    
<body>
    <div class="container-fluid ajuste dibujos" id="dibujos">
      <h1><i class="fas fa-bomb"></i>Nuestras creaciones</h1>
      <div class="row">
         <div class="col-sm">
         <a data-fancybox="gallery" href="img/pastel4.png"><img class="img-fluid" src="img/pastel4.png" height="100"></a>
            </div>
          <div class="col-sm">
          <a data-fancybox="gallery" href="img/pastel5.png"><img class="img-fluid" src="img/pastel5.png"></a>
            </div>
          <div class="col-sm">
          <a data-fancybox="gallery" href="img/pastel6.png"><img class="img-fluid" src="img/pastel6.png"></a>
            </div>
         </div>
         
         <br>
         
          <div class="row">
         <div class="col-sm">
         <a data-fancybox="gallery" href="img/pastel.png"><img class="img-fluid" src="img/pastel.png"></a>
         </div>
          <div class="col-sm">
         <a data-fancybox="gallery" href="img/pastel2.png"><img class="img-fluid" src="img/pastel2.png"></a>
            </div>
          <div class="col-sm">
          <a data-fancybox="gallery" href="img/pastel3.png"><img class="img-fluid" src="img/pastel3.png"></a>
            </div>
         </div>
         <br>
     </div>
     </div>
     </div>
     <script>
    
        
    $(document).ready(function(){
        
        $('#toggle').click(function(){
          $('.toggle').toggle('slow','swing'); 
        });
        
        $(".fancybox").fancybox({
            loop:true,
            
        });
        
        $('.owl-carousel').owlCarousel({
            loop: true,
            autoplay: true,
            autoplayTimeout: 1000,
            items: 6
        });
        
        $('.scroll-link').on('click', function(event){
          event.preventDefault();
        var id =$(this).attr('href');
         
        irId(id,1000)    
        });
        
      function irId(id, velocidad){
          var compensar = 50;
          var fcompensar = $(id).offset().top - compensar;
          $('html,body').animate({scrollTop:fcompensar},velocidad);
      }
        
    });   
    //Llamar al boton 
    boton = document.getElementById("subir");  
        
    //Muestra el boton cuando el us se desplace
    
    window.onscroll = function(){aparecerbtn()};  
    
    function aparecerbtn(){
        if(document.body.scrollTop > 200 || document.documentElement.scrollTop > 200){ boton.style.display = "block";
            
        }else{
                boton.style.display = "none";
        }
    } 
        
   $(document).ready(function () {

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });

});     
      
    </script>   
    <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
       
</body>