@if(Session::has('info'))
<div class="lert alert-info alert-dismissible fade show" role="alert">
  <strong>¡Atención!</strong> {{Session::get('info')}}
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

@if(Session::has('alert'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
  <strong>¡Atención!</strong> {{Session::get('alert')}}
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif


@if(Session::has('exito'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>¡Atención!</strong> {{Session::get('exito')}}
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
<script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
