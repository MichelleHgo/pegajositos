@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="card">
            <div class="card-header">Editar Pastel</div>

            <div class="card-body">
                <form method="POST" action="{{  route('tareas.update', $task->id)  }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    
                    <div class="form-group">

                        <label>Nombre del Cumpleañero</label>
                        <input type="text" name="title" class="form-control" value="{{ $task->title }}" required="">
                   
                    </div>

                    <div class="form-group">

                        <label>Fecha de Cumpleaños</label>
                        <input type="date" name="deadline" class="form-control" value="{{ $task->deadline }}">
                   
                    </div>

                    <div class="form-group">

                        <label>Especificaciones</label>
                        <textarea class="form-control" name="description" rows="5"> {{ $task->description }} </textarea>
                   
                    </div>
                    <div class="form-group">
						<label>Sabor</label>
							<select class="form-select"  name="sabor" aria-label="Default select example">
							<option selected>Selecciona tu sabor</option>
							<option value="1">Vainilla</option>
							<option value="2">Fresa</option>
							<option value="3">Chocolate</option>
							</select>
					</div>
                   
                    
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                    <a href="{{ route('tareas.index') }}" class="btn btn-outline-dark">Cancelar</a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>