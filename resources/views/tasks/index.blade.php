@extends('layouts.app')

@section('content')
<!-- Iconos fontawesome-->       
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">  

<div class="wrapper">
<nav id="sidebar">
         <div class="sidebar-header" href="{{ route('home')}}">
           <a href="{{ route('home')}}"> <img  class="img-fluid michotitulo animate__animated animate__shakeX" src="img/logo.png" >
       </a> </div>

        <ul class="list-unstyled components">
            
            <li>
                <a href="{{ route('home')}}"><i class="fas fa-home"></i> Inicio</a>
            </li>
           
          <li>
                <a href="{{ route('proyectos.index')}}"><i class="fas fa-book-open"></i> Catálogo</a>
            </li>
            <li>
                <a href="{{ route('tareas.index')}}"><i class="fas fa-birthday-cake"></i> Crea tu pastel</a>
            
              </li>
              @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
 <!--<li>
                <a href="#"><i class="far fa-address-book"></i> Contacto</a>-->
        </ul>

    </nav>
<div class="container ">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card shadow p-3 mb-5 bg-body rounded">
                <div class="card-header">PASTELES</div>
                

                <div class="card-body fondowall">
                    <a href="{{  route('tareas.create')  }}" class="btn btn-primary mb-3 rounded-pill"> <ion-icon name="add-circle"></ion-icon> Nuevo Pastel</a>
                </div>
                <table class="table table-sm ">
                  <thead>
                    <tr>
                      
                      <th scope="col">Nombre del Cumpleañero</th>
                      <th scope="col">Fecha de Cumpleaños</th>
                      <th scope="col">Descripción</th>
                      <th scope="col">Sabor</th>
                      <th scope="col">Usuario</th>
                      <th scope="col">Estado</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($tasks as $task)
                    <tr>
                      
                      <td>{{ $task->title }}</td>
                      <td>{{ $task->deadline }}</td>
                      <td>{{ $task->description }}</td>
                      <td>{{ $task->sabor }}</td>
                      <td>{{ $task->user->name }}</td>
                      <td>@if($task->is_complete == false)
                      <span class="badge badge-warning">Entregado </span>
                      @else
                      <span class="badge badge-success">No entregado</span>
                      @endif
                      </td>
                      <td>@if($task->is_complete == false)
                      <a href="{{ route('tareas.status', $task->id) }}" class="btn btn-outline-success btn-sm" data-toggle="tooltip" data-placement="top" title="Completar"><ion-icon name="checkbox-outline"></ion-icon></a>
                      @endif
                      <a href="{{ route('tareas.edit', $task->id) }}" class="btn btn-outline-info btn-sm" data-toggle="tooltip" data-placement="top" title="Editar"><ion-icon name="create"></ion-icon></a>
                      <form method="POST" style="display: inline-block;" action="{{ route('tareas.destroy', $task->id) }}">
                          {{ csrf_field() }}
                          {{ method_field('DELETE') }}
                          <button type="submit" data-toggle="tooltip" data-placement="top" title="Borrar" class="btn btn-danger btn-sm"><ion-icon name="close-circle-outline"></ion-icon></button>
                      </form>
                    </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
<script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
